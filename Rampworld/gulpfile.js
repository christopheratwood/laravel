'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const gzip = require('gulp-gzip');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const typescript = require('gulp-typescript');
const replace = require('gulp-replace');

const input = {
  public: {
    rwcui: './resources/assets/public/sass/rwcui.sass',
    ts: './resources/assets/public/ts/**/*.ts',
    sass: './resources/assets/public/sass/**/*.sass'
  }
}
const output = {
  public: {
    css: './public/public/css',
    js: './public/public/js',
    docs: './public/public/docs'
  },
  admin: {
    css: './public/admin/css',
    js: './public/admin/js',
    docs: './public/adminnpm /docs'
  }
}

const sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded'
}

const autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};


gulp.task('sass-public', ['clean-css-public'], function () {
  return gulp.src(input.public.rwcui)
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest(output.public.css))
    .pipe(cleanCSS({
      compatibility: 'ie8',
      debug: true
    }, function (details) {
      console.log(details.name + ': ' + details.stats.originalSize);
      console.log(details.name + ': ' + details.stats.minifiedSize);
    }))
    .pipe(rename({
      extname: '.min.css'
    }))
    .pipe(gulp.dest(output.public.css))
});

/* Typescript */
gulp.task('typescript-public', ['clean-js-public'], function () {
  return gulp.src(input.public.ts)
    .pipe(typescript({
      target: "es5",
      noImplicitUseStrict: false,
      sourceMap: false,
      declaration: false,
      module: "amd",
      removeComments: true
    }))
    .pipe(replace(/\\t|\\n/g, ''))
    .pipe(uglify())
    .pipe(gulp.dest(output.public.js))
});

gulp.task('watch-sass-public', function () {
  return gulp
    .watch(input.public.sass, ['sass'])
    .on('change', function (event) {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

gulp.task('watch-ts-public', function () {
  return gulp
    .watch(input.public.ts, ["typescript"])
    .on('change', function (event) {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});


gulp.task('clean-css-public', function () {
  gulp.src(output.public.css)
    .pipe(clean());
});

gulp.task('clean-js-public', function () {
  gulp.src(output.public.js)
    .pipe(clean());
});

gulp.task('default', ['sass-public', 'typescript-public'], function () { });
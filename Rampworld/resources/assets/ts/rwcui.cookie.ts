declare var define: RequireDefine;
declare var $: any;

define(['jquery', 'cookieC'], function ($: any, cookie: any) {
	const hasCookie: any = $.cookie('_rwc_cc');
	const init = function (): void {
		if (hasCookie === undefined) {
			$('body').append('<div id="cookie-banner"><p><i class="glyphicon glyphicon-info-sign"></i> This site uses cookies to enhance your viewing experience.<a href="//www.rampworldcardiff.co.uk/policies" class="btn btn-sm btn-success">Learn more</a> <button class="btn btn-sm btn-default" id="closeCookie">Close</button></p></div>');
		}

		$('body').on('click', '#closeCookie', function (e) {
			setCookie();
			$('#cookie-banner').fadeOut();
		})
	};
	/**
	 * Set cookie
	 */
	const setCookie = function (): void {
		$.cookie('_rwc_cc', 'true', { expires: 14 });
	}
	return {
		init: init
	}
});
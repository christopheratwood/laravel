declare var define: RequireDefine;
declare var $: any;

define(['jquery', 'moment', 'fullcalandar', 'rwcui.loading'], function ($: any, moment: any, fullCalendar:any, loading: any) {
	let config: any = {
		date: ($('#calendar').data('default-date') != undefined) ? moment($('#calendar').data('default-date'), 'YYYY-MM-DD') : moment()
	};
	const ajaxurl:String = "https://www.rampworldcardiff.co.uk/wp-content/themes/rampworld/modules/ajax.php";
	const _0xdfbe=["bookingBot","q-LWzawVy#?4_sT#ewB7ktak6-__XnAbca*#ULE+5MeR9AHJ7p5LSBW+rHkv%--="];

	const init = function (): void {

		render();
		loading.destroy($('#calendar'));
		loadSession(config.date);
		$('body').on('click', '.select-session', function (event) {
			event.preventDefault();
			select($(this));
		})
	};
	const render = function (): void {
		$('#calendar').append('<div id="fullcalandar" class="col-lg-8 col-md-6 col-sm-12 "></div><div id="dayView" class="col-lg-4 col-md-6 col-sm-12"><div class="card "><div class="card-content"><p id="event_name">Loading Sessions</p></div><div class="card-action" style="min-width: 250px; postition: relative"></div></div></div >');

		$('#fullcalandar').fullCalendar({
			defaultDate: config.date,
			firstDay: 1,
			header:{
				right: 'prev,next'
			},
			viewRender: function () {

				if (!moment().isSame(config.date, 'day')) {
					$('.fc-today.fc-state-highlight').removeClass('fc-today').removeClass('fc-state-highlight');
					$('*[data-date="' + config.date.format('YYYY-MM-DD') + '"]').addClass('fc-today').addClass('fc-state-highlight');
				}
				$('.fc-other-month').removeClass('fc-other-month').removeClass('other-month');
				$('.fc-past').removeClass('fc-past').addClass('other-month').addClass('fc-other-month');
			},
			dayClick: function (date, event, view) {
				if (moment().diff(date, 'days') <= 0) {
					$('.fc-today.fc-state-highlight').removeClass('fc-today').removeClass('fc-state-highlight');
					$('*[data-date="' + $(this).data("date") + '"]').addClass('fc-today').addClass('fc-state-highlight');
					$('html, body').animate({
						scrollTop: $('#dayView').offset().top - 150
					}, 400);


					loadSession(date);

				}

			},
			loading: function (b) {
			},
		});
	};
	const loadSession = function (date: any) {


		var date_yy = date.format('YYYY-MM-DD');
		var full_date = date.format('dddd Do  MMMM YYYY');
		var day = date.format('dddd') + ' - Sunday';

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			dataType: 'JSON',
			
			data: {
				date: date_yy,
				action: 'getTodaysSessions'
			},
			headers: {
				"Authorization": "Basic " + btoa(_0xdfbe[0]+":"+_0xdfbe[1])
			},
			beforeSend: function (xhr) {
				$('#dayView').html('<div class="card"><div class="card-content"><p id="event_name">' + date.format('dddd Do  MMMM YYYY') + '</p></div><div class="card-action"></div></div>');
				loading.add($('.card-action'), 'Please Wait')


			},
			success: function (rawData) {
				
				loading.destroy($('.card-action'));
				let data: any = $.parseJSON(rawData);

				if(data.length == 0 || data.length === undefined) {
					$('#dayView').html(`<div class="card">
						<div class="card-content">
							<p id="event_name">${full_date}</p>
						</div>
						<div class="card-action">
							<p class="lead">Please select a session</p>
							<p class="alert alert-danger">RampWorld Cardiff is closed on ${full_date}</p>
						</div>
					</div>`);
				} else {
					$('#dayView').html('<div class="card"><div class="card-content"><p id="event_name">' + full_date + '</p></div><div class="card-action"><p class="lead">Please select a session</p><div id="sessions_list" class="row"></div></div></div > ');

					for (let i: number = 0; i < data.length; i++) {
						let session_id: number = ((data[i][1].hasOwnProperty(1)) ? data[i][1][0][1] : null);
						let name: string = ((data[i][1].hasOwnProperty(1)) ? data[i][1][1][1] : null);
						let start: string = ((data[i][1].hasOwnProperty(2)) ? data[i][1][2][1] : null);
						let end: string = ((data[i][1].hasOwnProperty(3)) ? data[i][1][3][1] : null);
						let private: any = ((data[i][1].hasOwnProperty(4)) ? data[i][1][4][1] : null);
						let beginner: any = ((data[i][1].hasOwnProperty(5)) ? data[i][1][5][1] : null);
						let consec: any = ((data[i][1].hasOwnProperty(6)) ? data[i][1][6][1] : null);
						let total: number = ((data[i][1].hasOwnProperty(7)) ? data[i][1][7][1] : null);
						let cost: string = ((data[i][1].hasOwnProperty(8)) ? data[i][1][8][1] : null);
						let selected: any = ((data[i][1].hasOwnProperty(9)) ? data[i][1][9][1] : null);
						let isConsecutive: string = ((data[i][1].hasOwnProperty(10)) ? data[i][1][10][1] : null);
						let online: any = ((data[i][1].hasOwnProperty(11)) ? data[i][1][11][1] : null);
						let description: any = ((data[i][1].hasOwnProperty(12)) ? data[i][1][12][1] : null);

						let eventDiscipline = false;
						let restrictionText = '';

	
						let type: string = ((data[i][1][0][0] == 'event_id') ? 'event' : ((consec > 1) ? 'consecutive' : ((data[i][1][0][0] == 'holiday_session_id' ? 'holiday' : (data[i][1][0][0] == 'weekpass_id') ? 'weekpass':'session'))));

						let same_date: boolean = ((date.isSame(moment(), 'day')) ? true : false);
						let valid_date:boolean = false;
						if(same_date == true) {
							if(moment().format('HH:MM') < (start)) {
								valid_date = true
							}
						} else {
							valid_date = true;
						}
	
						let totalRemaining: number = 0;
						if (online == null)
							totalRemaining = 150 - total;
						else
							totalRemaining = 170 - total;
						if (totalRemaining < 0)
							totalRemaining = 0;

						if(type == 'event' && private.length == 6 && private != "000000") {
							eventDiscipline = true;


							let disciplines:any[string] = [];

							if( private.charAt(0) == 1 ) {
								disciplines.push('scooter');
							}
							if( private.charAt(1) == 1 ) {
								disciplines.push('BMX');
							}
							if( private.charAt(2) == 1) {
								disciplines.push('MTB');
							}
							if( private.charAt(3) == 1) {
								disciplines.push('skateboard');
							}
							if( private.charAt(4) == 1 ) {
								disciplines.push('inline');
							}
							if( private.charAt(5) == 1) {
								disciplines.push('spectator');
							}
							disciplines.sort();
							
							for(var d:number = 0; d < disciplines.length; d++) {
								restrictionText += disciplines[d];

								if(d == disciplines.length -2 ) {
									restrictionText += ', and ';
								} else if(d != disciplines.length -1 ) {
									restrictionText += ', ';
								}
							} 
						}
						

	
						if (data[i][1][0][0] != 'weekpass_id') {

							const processButton = `
								<button type="submit" 
										class="float-right btn ${((selected != null) ? 'btn-primary' : 'btn-default hover-success ')} select-session"
										data-session="${start} - ${end}"
										data-session-beginner="${beginner}"
										data-session-name="${name}"
										data-session-cost="${cost}"
										data-event-restrictions="${((type == 'event') ? restrictionText: false)}"
										data-is-event="${((type == 'event') ? session_id: false)}">
									<i class="glyphicon glyphicon-ok"></i> ${((selected != null) ? 'Selected' : 'Select')}
								</button>`;

							$('#sessions_list').append('<div class="col-xs-12"><div class="col-xs-12" style="padding-left: 0; padding-right:0; text-align:left;"><p class="session_name event">' + ((isConsecutive == null) ? name : '') + ((beginner && type != 'event') ? ' <a href="#" class="badge badge-danger" title="Beginner Session" data-toggle="popover" data-placement="bottom" data-trigger="focus" data-content="This is a beginner session, therefore, only beginners are allowed to attend this session"> Warning !</a>' : '') + '</p> ' + (
								(valid_date) ?
									`<form class="" action="https://www.rampworldcardiff.co.uk/online-booking/book?stage=1" method="post">
										<p class="session-name"> ${start} - ${end}` + 
											((valid_date) ? '  <span class="label ' + formatLabel(totalRemaining, type) + '">' + totalRemaining + ' Left</span>' : '') +
											`${processButton}
										</p>
										<input type="hidden" name="session_start" value="${start}">
										<input type="hidden" name="session_end" value="${end}">
										<input type="hidden" name="session_date" value="${date.format('DD-MM-YYYY')}">
										<input type="hidden" name="session_cost" value="${cost}">
										<input type="hidden" name="session_name" value="${name}">
										<input type="hidden" name="session_beginner" value="${beginner}">
										<input type="hidden" name="stage" value="1">
										<input type="hidden" name="session_type" value="${type}">
										<input type="hidden" name="session_id" value="${session_id}">
										<input type="hidden" name="session_disciplines" value="${((type == 'event') ? restrictionText: false)}">
										<input type="hidden" name="session_pass_exclusion" value="${((type == 'event') ? consec: false)}">
									</form>` : 
									`<p class="session_name">${start} - ${end}
										<a class="float-right btn ${((type == 'event') ? 'btn-info' : 'btn-default')} disabled">
											<i class="glyphicon glyphicon-remove"></i>  Unable to book</a>`) + '</p></div></div>');
						} else {
							let monday: any = moment(start);
							let sunday: any = moment(end, 'YYYY-MM-DD');

							const processButton = `
								<button type="submit"
										class="float-right btn ${((selected != null) ? 'btn-primary' : 'btn-default hover-success ')} select-session"
										data-session="${day}
										data-session-beginner="${beginner}"
										data-session-name="${name}"
										data-session-cost="${cost}">
									<i class="glyphicon glyphicon-ok"></i> ${((selected != null) ? 'Selected' : 'Select')}
								</button>`;


							$('#sessions_list').append(`
								<div class="col-xs-12">
									<div class="col-xs-12" style="padding-left: 0; padding-right:0; text-align:left;">
										<p class="session_name event">${((isConsecutive == null) ? name : '')} <a href="#" class="badge badge-danger" title="${name}" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-content="The week only runs until the last day of the week. This pass will end on the ${sunday.format('dddd [the] Do MMMM YYYY')}"> Warning !</a></p>` + (
								(valid_date) ?
									`<form class="" action="https://www.rampworldcardiff.co.uk/online-booking/book?stage=1" method="post">
										<p class="session-name">
											${day} ${((valid_date) ? ' <span class="label ' + formatLabel(totalRemaining, type)+ '">' + totalRemaining + ' Left</span>' : '')}
											${processButton}
										</p>
										<input type="hidden" name="session_start" value="${start }">
										<input type="hidden" name="session_end" value="'${end }">
										<input type="hidden" name="session_date" value="${date.format('DD-MM-YYYY')}">
										<input type="hidden" name="session_cost" value="${cost}">
										<input type="hidden" name="session_name" value="${name}">
										<input type="hidden" name="session_beginner" value="${beginner}">
										<input type="hidden" name="stage" value="1">
										<input type="hidden" name="session_type" value="${type}">
										<input type="hidden" name="session_id" value="${session_id}">
										<input type="hidden" name="session_disciplines" value="${((type == 'event') ? restrictionText: false)}">
										<input type="hidden" name="session_pass_exclusion" value="${((type == 'event') ? consec: false)}">
									</form>` :
									`<p class="session_name">
										${day}
										<a class="float-right btn ${((type == 'event') ? 'btn-info' : 'btn-default')} disabled">
											<i class="glyphicon glyphicon-remove"></i>  Unable to book
										</a>`) + '</p></div></div>');
						}
					}
				}
				

				$("[data-toggle='popover']").popover();
				$("[data-toggle='popover']").on('click', function (e) { e.preventDefault(); return true; });
			},
			error: function (e) {
				console.log(e);
			}
		});
	};
	const select = function (element: any, self: any = null): void {
		let height: number = $('.card-action').height() + 66
		$('html, body').animate({
			scrollTop: $('#dayView').offset().top - 60
		});

		let controls = '<a class="btn btn-success btn-block btn-lg" id="book">Book</a>';
		let restrictionHTML = '<div class="alert alert-danger">';
		let hasRestrictions = false;

		if(element.data('session-beginner') == '1') {
			hasRestrictions = true;
			restrictionHTML += '<p><strong>Beginner Session Only</strong> This session is <strong>strictly</strong> for beginners only.</p>';
		}
		if(element.data('weekpass') !== undefined) {
			hasRestrictions = true;
			restrictionHTML += `<p>${element.data('weekpass')}</p>`;
		}
		if( element.data('event-restrictions') != false ) {
			hasRestrictions = true;
			restrictionHTML += `<p>This event is only for <strong>${element.data('event-restrictions')}</strong>. No other disciplines will be allowed to attend.</p>`;
		}
		if( !hasRestrictions ) {
			restrictionHTML = '<div class="alert alert-success">No restrictions</div>';
		} else {
			restrictionHTML += '</div>'
		}
		if( element.data('is-event') !== false ) {
			controls += `
				<a class="btn btn-default" style="width: calc(50% - 10px);" href="https://www.rampworldcardiff.co.uk/events/${element.data('is-event')}/${element.data('session-name').toLowerCase().replace(/ /g, '-')}">View event</a>
				<a class="btn btn-default" style="width: calc(50% - 10px);" id="goToSessions">Go Back</a>`;
		} else {
			controls += `<a class="btn btn-default btn-block" id="goToSessions">Go Back</a>`;
		}



		$('.card-content').animate({ minHeight: height + 'px' }, 300);
		$('#event_name').animate({ fontSize: '22px' }, 200, function () {
			$('#event_name').after(`
			<div id="session-overview">
				<div style="margin-top:20px;">
					<p class="lead" style="font-size:22px;">${element.data('session-name')}</p>
				</div>
				<div style="margin-top:20px;">
					<p class="lead" style="font-size:22px;">£${element.data('session-cost')}</p>
				</div>
				<div style="margin-top:20px;">
					<p class="lead" style="font-size:18px;">Session Times</p><p>${element.data('session')}</p>
				</div>
				<div style="margin-top:20px;">
					<p class="lead" style="font-size:18px;">Restrictions</p>
					${restrictionHTML}
				</div>
				<div style="margin-top:20px;">
					${controls}
				<div>
			</div>`);
		});

		$('.card-action').animate({ height: '0px' }, 300, function () {
			$('.card-action').css({ display: 'none' });
		});
		$('body').on('click', '#goToSessions', function (e) {
			e.preventDefault();
			$(this).remove();
			$('.card-content').animate({ minHeight: '66px' }, 300);
			$('#session-overview').fadeOut().remove();
			$('.card-action').css({ display: 'block' });
			$('.card-action').animate({ height: height - 51 + 'px' }, 300);

		});
		$('body').on('click', '#book', function (e) {
			$('#session-overview').fadeOut().remove();
			loading.add($('.card-content'), 'Please Wait', 'white');

			element.parent().parent().submit();


		});
	};
	
	const formatLabel = function(totalRemaining:number, type = "session") {
		if( type == "session" ) {
			return ((totalRemaining > 90) ? 'label-success' : ((totalRemaining > 70) ? 'label-info' : ((totalRemaining > 50) ? 'label-warning' : 'label-danger')));
		} else {
			return ((totalRemaining > 100) ? 'label-success' : ((totalRemaining > 80) ? 'label-info' : ((totalRemaining > 50) ? 'label-warning' : 'label-danger')));
		}

	}
	return {
		init: init,
		render: render,
		loadSession: loadSession,
		select: select
	}

});
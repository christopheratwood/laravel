declare var define: RequireDefine;
declare var $: any;
declare var moment: any;

define(['jquery', 'rwcui.error.rendering'], function ($: any, error: any) {
  const roots: any = {
    live: '',
    dev: '//127.0.0.1/'
  }
  const init = function () {
    let errorCount: number = -1;
    $('#signIn').off().on('click', function (e) {
      if ($('#form_username').val().length == 0) {
        error.add('username', 'Please enter your email/ username.');
        errorCount++;
      } else {
        error.clear('username');
        errorCount--;
      }
      if ($('#form_password').val().length == 0) {
        error.add('password', 'Please enter your password.');
        errorCount++;
      } else {
        error.clear('password');
        errorCount--;
      }
      if (errorCount < 0) {
        $('#signIn').html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" id="loading"></span> Checking details');
        $('#signIn').addClass('disabled');
        $.ajax({

          url: roots.live + "wp-content/themes/rampworld/modules/thirdparty/ajax.php",
          type: 'POST',
          data: {
            action: 'verify_account',
            username: $('#form_username').val(),
            password: $('#form_password').val()

          },
          beforeSend: function () {
            $('#form_password').val('')
          },
          success: function (data) {
            console.log(data);
            let result: any = JSON.parse(data);

            if (result.errorCode !== undefined) {
              $('#signIn').html('Sign in');
              $('#signIn').removeClass('disabled');
              if (result.errorCode == 1) {
                $('#signIn').after(`<div class="alert alert-danger"><strong>${result.errorMessage}</strong> Please try again</div>`);
              }
              if (result.errorCode == 2) {
                $('#signIn').after(`<div class="alert alert-danger"><strong>${result.errorMessage}</strong> Please contact customer support.</div>`);
              }

            } else {
              $('#signIn').html('Sign in');
              $('#signIn').removeClass('disabled');
              $('#form_username').val('');

              $('#signInForm').addClass('hidden');
              $('#signInForm').after(`
                <div id="loggedIn"><p class="lead">Welcome back, ${result.name}</p>
                <div>
                <p> You are currently logged in, if you wish not log out of this account, please click the button below</p>
                    <a id="logout" class="btn btn-danger">Logout</a>
                    <input type="hidden" id="account_id" name="account_id" value="${result.account_id}">
                    </div></div>`)
            }

          }
        });

      }

    });
    $('body').off().on('click', '#logout', function (e) {
      $.ajax({

        url: roots.dev + "wp-content/themes/rampworld/classes/thirdparty/ajax.php",
        type: 'POST',
        data: {
          action: 'logout',
          account_id: $('#account_id').val()

        },
        beforeSend: function () {
          $('#signInForm').removeClass('hidden');
          $('#loggedIn').remove();
        },
        success: function (data) {
          console.log(data);
        }
      });
    })
  }
  return {
    init: init
  }
});
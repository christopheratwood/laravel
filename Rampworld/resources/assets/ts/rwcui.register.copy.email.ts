declare var define: RequireDefine;
declare var $: any;

define(['jquery'], function ($: any) {
	const init = function (): void {

		$('#copyParticipantEmail').on('click', function () {

			$('#form_consent_email').val($('#copyParticipantEmail').data('email'));
		});
	}
	return {
		init: init
	}
}); 
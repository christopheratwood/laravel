declare var define: RequireDefine;
declare var $: any;

define(['jquery', 'rwcui.functions', 'rwcui.booking.validation'], function ($: any, functions: any, validation: any) {


	const ajaxurl:String = "https://www.rampworldcardiff.co.uk/wp-content/themes/rampworld/modules/ajax.php";
	const _0xdfbe=["bookingBot","q-LWzawVy#?4_sT#ewB7ktak6-__XnAbca*#ULE+5MeR9AHJ7p5LSBW+rHkv%--="];
	
	const init = function (): void {

		$('#form-next').on('click', function (e) {

			e.preventDefault();
			checkStage($(this).data('current'), $(this).data('next'));
		});
		$('#form-prev').on('click', function (e) {
			e.preventDefault();
			checkStage($(this).data('current'), $(this).data('next'));
		});
		$('#checkout-btn').off().on('click', function (e) {
			validation.validate('tandcs', $('#tandc'), { 'checked': true }, 'Terms and Conditions', true, true);
			if (validation.calcErrors() == 0) {
				$(this).addClass("btn btn-primary btn-lg btn-block");
				$(this).html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" id="loading"></span> Processing');
				$(this).addClass('disabled');
				return true;
			} else {
				return false;
			}

		});
	};
	const checkStage = function (current: number, next: number): void {

		switch (current) {
			case 2:
				
				functions.capitalise('form_booking_name');
				validation.validate('booking', $('#form_booking_name'), { length: [3, 100], characters: 'letters' }, null, !0);
				validation.validate('booking', $('#form_booking_number'), { length: [10, 17], characters: 'numbers' }, 'Phone number', !0);
				validation.validate('booking', $('#form_booking_email'), { length: [1, 255], email: !0 }, null, !0);

				if (validation.calcErrors() == 0)
					$('#booking-form').attr('action', '?stage=' + current + '&next=' + next).submit();

				break;
			case 3:
				$.ajax({
					type: "POST",
					url: ajaxurl,
					data: {
						action: 'get_total_participants'


					},
					beforeSend: function (xhr: any) {
						xhr.setRequestHeader ("Authorization", "Basic " + btoa( _0xdfbe[0]+ ":" + _0xdfbe[1]));
					},
					success: function (data) {
						var total = JSON.parse(data);

						if (total > 0) {
							$('#booking-form').attr('action', '?stage=' + current + '&next=' + next).submit();
						} else {
							$('#main_page_errors').empty();

							$('#main_page_errors').append('<b>Errors (1)</b><br>');
							$('#main_page_errors').addClass('visible block');
							$('#main_page_errors').append('Please choose at least 1 member.<span style="display:block;margin-top:10px"></span>');


						}
					}
				});
		}
	}

	return {
		init: init,
		checkStage: checkStage
	}
});
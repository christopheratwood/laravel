declare var define: RequireDefine;
declare var $: any;

define(['jquery'], function ($: any) {

	const config = {
		host: 'https://res.cloudinary.com/rampworld-cardiff/image/upload',
		sizes: {
			lg: '/w_598',
			md: '/w_555',
			sm: '/w_345',
			xs: '/w_368',
			ms: '/w_455'
		},
		fullscreen:{
			lg: '/w_598',
			md: '/w_737',
			sm: '/w_636',
			xs: '/w_535',
			ms: '/w_434',
			mms: '/w_343',
			mmms: '/w_242'
		}
	};
	const init = function(): void {
		$('#product-gallery #primary').on('click', function() {
			
			preview();
		});


		$('body').off().on('click', '#product-gallery #product-gallery-thumbnails img', function( this:any, evt:any ) {
			evt.preventDefault();
			changePrimary( $(this) );
		});
	};
	const preview = function():void {
		const image = $('#product-gallery #primary').data('image-path');

		$( 'body' ).append(
			`<div class="modal fade in product-gallery" tabindex="-1" role="dialog" id="product-preview">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<img srcset="
								${config.host}${config.fullscreen.lg}${image}  768w,
								${config.host}${config.fullscreen.md}${image}  750w,
								${config.host}${config.fullscreen.sm}${image}  500w,
								${config.host}${config.fullscreen.xs}${image}  500w,
								${config.host}${config.fullscreen.ms}${image}  400w,
								${config.host}${config.fullscreen.mms}${image}  300w,
								${config.host}${config.fullscreen.mmms}${image}  200w"
							src="${config.host}${config.sizes.lg}${image}"
							sizes="(max-width: 768px) calc(100vw - 20px), (min-width: 768px) 600px"></div>
				</div>
			</div>`);
			$('#product-preview').modal('show');
		
		$('#product-preview').on('hidden.bs.modal', function (e:any){
			$('#product-preview').remove();
		});
	};

	const changePrimary = function( $this:any ): void {
		$('#product-gallery #primary').data( 'image-path', $this.data( 'image-path' ) );
		$('#product-gallery-thumbnails img.active').removeClass( 'active' );
		$this.addClass( 'active' );
		$('#product-gallery #primary').attr('srcset', `
			${config.host}${config.sizes.lg}${$this.data('image-path')}  700w,
			${config.host}${config.sizes.sm}${$this.data('image-path')}  550w,
			${config.host}${config.sizes.xs}${$this.data('image-path')}  420w,
			${config.host}${config.sizes.ms}${$this.data('image-path')}  300w`);
			$('#product-gallery #primary').attr('src',`${config.host}${config.sizes.lg}${$this.data('image-path')}`);
	}

	return {
		init: init
	}
})
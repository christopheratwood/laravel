declare var define: RequireDefine;
declare var $: any;

define(['jquery', 'rwcui.booking.validation', 'rwcui.error.rendering'], function ($: any, validation: any, error: any) {

	let member: any = {
		'id': -99,
		'name': '',
		'phone_number': '',
		'email': ''
	}

	const ajaxurl:String = "https://www.rampworldcardiff.co.uk/wp-content/themes/rampworld/modules/ajax.php";
	const _0xdfbe=["bookingBot","q-LWzawVy#?4_sT#ewB7ktak6-__XnAbca*#ULE+5MeR9AHJ7p5LSBW+rHkv%--="];

	const init = function (): void {
		$('#form_participant_member_number').on('keydown', function (e) {
			if (e.keyCode == 13) {
				$('.results').empty();

				validation.validate('booking', $('#form_participant_member_number'), { length: [1, 8], characters: 'numbers' }, 'Membership Number', !0);

				if ($('#form_participant_member_number').val() >= 0)
					getMetadataParticipant($('#form_participant_member_number').val(), true);
				return false;

			}
		});
		$('#form_booking_member_number').on('keyup', function (e) {
			if (e.keyCode == 13) {

				$('.results').empty();

				validation.validate('booking', $('#form_booking_member_number'), { length: [1, 8], characters: 'numbers' }, 'Membership Number', !0);

				if ($('#form_booking_member_number').val() >= 0)
					getMetadata($('#form_booking_member_number').val(), true);
				return false;
			}
		});
		$('#findParticipant').off().on('click', function (e) {

			$('.results').empty();
			validation.validate('booking', $('#form_participant_member_number'), { length: [1, 8], characters: 'numbers' }, 'Membership Number', !0);

			if ($('#form_participant_member_number').val() >= 0)
				getMetadataParticipant($('#form_participant_member_number').val(), true);

		});

		$('#findMember').off().on('click', function (e) {
			$('.results').empty();
			if ($('#form_booking_member_number').val() <= 0) {
				validation.validate('booking', $('#form_booking_member_number'), { length: [1, 8], characters: 'numbers' }, null, !0);

			} else {
				error.clear('booking_member_number');
				getMetadata($('#form_booking_member_number').val());
			}
			return false;
		});
		$('body').off('click', '#correctDetails');
		$('body').on('click', '#correctDetails', function () {
			correctDetails();
		});
		$('body').off('click', '#incorrectDetails');
		$('body').on('click', '#incorrectDetails', function () {
			incorrectDetails();
		});
		$('body').off('click', '#correctDetailsParticipant');
		$('body').on('click', '#correctDetailsParticipant', function () {
			addParticipant(member.id, true);
		});
		$('body').off('click', '#removeParticipant');
		$('body').on('click', '#removeParticipant', function () {
			let self: any = $(this);
			$.ajax({
				type: "POST",
				url: ajaxurl,
				data: {
					action: 'remove_participant',
					mid: self.data('member-id')
				},
				beforeSend: function (xhr) {
					xhr.setRequestHeader ("Authorization", "Basic " + btoa( _0xdfbe[0]+ ":" + _0xdfbe[1]));
				},
				success: function (data: any) {

					let status: any = JSON.parse(data);

					if (status != false) {
						let name: string = self.parent().find('.lead')[0]['innerText'];

						self.addClass('success');

						$('#total').text('£' + status.total_cost);
						$('#total_row').text('£' + status.total_cost);
						$('#participants_sidebar > tbody > tr').each(function (i, v) {
							if (i > 0)
								if ($(v).find('td')[0]['innerText'] == name)
									v.remove();
						});
						setTimeout(function () {
							self.parent().fadeOut(150, function () {
								self.parent().remove();
							})
						}, 730)
					} else {
						self.removeClass('disabled');
						self.html('<span class="glyphicon glyphicon-cross"></span> Remove')
					}
				}
			});

		})
	};
	const getMetadata = function (member_id: number, add: any = null) {
		if ((member_id != member.id) || add != null) {

			$.ajax({

				url: ajaxurl,
				type: 'POST',
				data: {
					action: 'check_booking_member',
					mid: member_id

				},
				beforeSend: function (xhr: any) {
					xhr.setRequestHeader ("Authorization", "Basic " + btoa( _0xdfbe[0]+ ":" + _0xdfbe[1]));
					$('#findMember').html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" id="loading"></span> Searching');
					$('#findMember').addClass('disabled');

				},
				success: function (data: any) {


					$('#findMember').removeClass('disabled');
					$('#findMember').html('Find Member');

					member.id = member_id;

					let h: any = JSON.parse(data);

					if (h == false) {
						error.add('booking_member_number', 'Oppsy, This membership number does not exist. Please try again.');

					} else {


						member.name = h[0]['forename'] + ' ' + h[0]['surname'];
						member.phone_number = h[0]['number'];
						member.email = h[0]['email'];

						$('#input_mid').attr('disabled', 'disabled');

						$('.results').empty();
						$('.results').append('<label class="col-md-4 col-ms-12 control-label reg required" >Are these details correct?</label></div><div class="col-ms-12"><p style="display: inline-block;">' + h[0]['forename'] + ' ' + h[0]['surname'] +'</p><a role=button" class="btn btn-primary btn-sm" id="correctDetails">Yes</a><a class="btn btn-default btn-sm" id="incorrectDetails">No</a></div>');


					}
				}
			});
		}
	};
	const getMetadataParticipant = function (member_id: number, add: any = null): void {
		if ((member_id != member.id) || add != null) {
			$.ajax({

				url: ajaxurl,
				type: 'POST',
				data: {
					action: 'check_participants',
					mid: member_id

				},
				beforeSend: function (xhr: any) {
					xhr.setRequestHeader ("Authorization", "Basic " + btoa( _0xdfbe[0]+ ":" + _0xdfbe[1]));
					if (add == null) {
						$('#findMember').html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" id="loading"></span> Searching');
						$('#findMember').addClass('disabled');
					} else {
						$('#addParticipant').html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" id="loading"></span> Searching');
						$('#addParticipant').addClass('disabled');
					}
				},
				success: function (data: any) {

					if (add == null) {
						$('#findMember').removeClass('disabled');
						$('#findMember').html('Find Member');
					} else {
						$('#addParticipant').removeClass('disabled');
						$('#addParticipant').html('Find Member');
					}

					member.id = member_id;
					let h: any = JSON.parse(data);

					if (h == false) {
						error.add('participant_member_number', 'Oppsy, This membership number does not exist. Please try again.');
					} else if (h == true) {
						error.add('participant_member_number', 'Oppsy, This member has already been added.');
					} else if (h == 'alreadyBooked') {
						error.add('participant_member_number', 'This member already have a session booked.');
					} else {


						member.name = h[0]['forename'] + ' ' + h[0]['surname'];
						member.phone_number = h[0]['number'];
						member.email = h[0]['email'];

						$('#input_mid').attr('disabled', 'disabled');

						$('.results').empty();

						$('.results').append('<label class="col-md-4 col-ms-12 control-label reg required" >Are these details correct?</label></div><div class="col-ms-12"><p style="display: inline-block;">' + h[0]['forename'] + ' ' + h[0]['surname'] + '</p><a role=button" class="btn btn-primary btn-sm" id="correctDetailsParticipant">Yes</a><a class="btn btn-default btn-sm" id="incorrectDetails">No</a></div>');



					}
				}
			});
		}
	}
	const correctDetails = function (): void {


		$('#form_booking_name').val(member.name);
		$('#form_booking_number').val(member.phone_number);
		$('#form_booking_email').val(member.email);

		validation.validate('booking', $('#form_booking_name'), { length: [3, 100], characters: 'letters' }, null, !0);
		validation.validate('booking', $('#form_booking_number'), { length: [10, 17], characters: 'numbers' }, 'Phone number', !0);
		validation.validate('booking', $('#form_booking_email'), { length: [1, 255], email: !0 }, null, !0);
		addParticipant(member.id, false);
		$('.results').empty();

	};
	const incorrectDetails = function (): void {
		reset();
		error.reset('booking_member_number');
		$('.results').empty();
		$('#form_booking_member_number').val('').focus();
		$('#form_booking_name').val('');
		$('#form_booking_number').val('');
		$('#form_booking_email').val('');

	};
	const addParticipant = function (member_id: number, participant: boolean) {
		$('#correctDetailsParticipant').html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" id="loading"></span> Adding');
		$('#correctDetailsParticipant').addClass('disabled');
		$('#incorrectDetails').remove();
		error.reset('participant_member_number');

		if (member_id != -99) {

			$.ajax({

				url: ajaxurl,
				type: 'POST',
				data: {
					action: 'add_participant',
					mid: member_id

				},
				beforeSend: function(xhr) {
					xhr.setRequestHeader ("Authorization", "Basic " + btoa( _0xdfbe[0]+ ":" + _0xdfbe[1]));
				},
				success: function (data) {

					reset();
					if (participant) {
						let member: any = JSON.parse(data);
						
						if (member == false) {
							error.add('participant_member_number', 'Oppsy, This membership number does not exist. Please try again.');
						} else if (member == true) {
							error.add('participant_member_number', 'Oppsy, This member has already been added.');
						} else if (member == 'alreadyBooked') {
							error.add('participant_member_number', 'This member either already has a session booked for your chosen date and time or has purchased a paid membership.');
						} else {
							$('#form_participant_member_number').val('');
							$('#participants').append('<div class="col-xs-6 col-ms-12"><p class="lead float-left">' + member.name + '</p><a id="removeParticipant" data-member-id="' + member.member_id + '" class="remove-btn"><span>remove</span><div class="icons"><i class="fa fa-times-circle"></i><i class="fa fa-check"></i></div></a></div>');
							$('#participants_sidebar').append('<tr><td>' + member.name + '</td><td>£' + member.cost + '</td></tr>');

							$("#pricing").addClass('updated');
							setTimeout(function (e) {
								$("#pricing").removeClass('updated');
							}, 750);
							$('#total').text('£' + member.total_cost);
							$('#total_row').text('£' + member.total_cost);

						}
					}
					$('.results').empty();
				}
			});
		}



	};
	const correctDetailsParticipant = function (): void {
		$.ajax({

			url: ajaxurl,
			type: 'POST',
			data: {
				action: 'add_participant',
				mid: member.id

			},
			beforeSend: function(xhr) {
				xhr.setRequestHeader ("Authorization", "Basic " + btoa( _0xdfbe[0]+ ":" + _0xdfbe[1]));
			},
			success: function (data) {
				reset();

				$('.results').empty();
				$('#form_booking_member_number').val('').focus();
			}
		});
	};
	const reset = function (): void {
		member.name = -99;
		member.name = '';
		member.phone_number = '';
		member.email = '';
	}
	return {
		getMetadata: getMetadata,
		init: init,
		getMetadataParticipant: getMetadataParticipant,
		correctDetails: correctDetails,
		addParticipant: addParticipant,
		incorrectDetails: incorrectDetails

	}

});
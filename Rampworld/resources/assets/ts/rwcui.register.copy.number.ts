declare var define: RequireDefine;
declare var $: any;

define(['jquery'], function ($: any) {
	const init = function (): void {

		$('#copyParticipantNumber').on('click', function () {

			$('#form_consent_number').val($('#copyParticipantNumber').data('number'));
		});
	}
	return {
		init: init
	}
}); 
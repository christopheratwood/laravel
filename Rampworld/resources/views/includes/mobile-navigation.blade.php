<nav role="navigation" class="wpmm-menu mm-menu mm-offcanvas mm-front mm-iconpanel mm-hasnavbar-top-4 mm-hasnavbar-bottom-1 mm-shadow-page mm-effect-slide-menu" id="mm-0">
    <button class="mm-tabstart" tabindex="0" type="button" aria-hidden="true"></button>
    <div class="mm-navbars-top">
        <div class="mm-navbar mm-navbar-size-3">
            <div class="wpmm-header-image">
                <svg class="mobile-logo" preserveAspectRatio="xMinYMin meet" width="206.04" height="95" version="1.1" viewBox="-1 -1.0000052892507796 210.04121971765812 99.00052796095714" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path id="a" d="m46.99 65c0.5 0 15.01 28.79 15.01 29.89 0 0.18-30 0.12-30 0 0-1 14.5-29.89 14.99-29.89z"></path><path id="k" d="m65 65h14.7c4.85 0 11.2 2.29 11.53 8.42s-6.72 10.16-7.27 10.51c-0.4 0.26 3.09 3.95 10.44 11.07h-14.7-14.7v-15-15z"></path><path id="f" d="m131 66h7.5 7.5v14.5 14.5h-7.5-7.5v-14.5-14.5z"></path><path id="d" d="m178.94 66h27.1v4.84 6.68h-2.15-3.27v3.42 4.81h-1.73-3.69v4.62 4.63h-8.13-8.13v-29z"></path><path id="m" d="m0 32.39h15 15v15.05 14.95l-7.76-7.24-7.76-7.24-7.24 6.86-7.24 6.95v-14.66-14.67z"></path><path id="e" d="m98 32.39h12.29l0.21 19.77 13.36 0.27v9.96h-25.86v-30z"></path><path id="g" d="m126.62 32.41c7.65-0.03 11.81-0.03 12.47 0 10.7 0.29 19.04 7.59 19.04 15.09 0 7.49-8.94 14.89-19.04 14.89h-12.47v-14.99-14.99z"></path><path id="n" d="m65.24 0.6c0.25 0 5.15 4.47 14.69 13.4 9.66-9.33 14.62-14 14.88-14 0.25 0 0.25 10 0 30h-29.57c-0.32-19.6-0.32-29.4 0-29.4z"></path><path id="h" d="m98 0h15c6.39 0 9.69 2.91 9.89 8.74 0.2 5.82-3.09 9.25-9.89 10.26v11h-15v-30z"></path><path id="o" d="m149 66h27.1v4.84 6.68h-2.15-3.27v3.42 4.81h-1.74-3.68v4.62 4.63h-8.13-8.13v-29z"></path><path id="l" d="m98 65.02c7.29-0.03 11.24-0.03 11.87 0 10.19 0.29 18.13 7.59 18.13 15.09 0 7.49-8.51 14.89-18.13 14.89h-11.87v-14.99-14.99z"></path><path id="p" d="m62 47.39c0 8.28-6.72 15-15 15s-15-6.72-15-15 6.72-15 15-15 15 6.72 15 15z"></path><path id="j" d="m65 32.39h14.7c4.85 0 11.2 2.29 11.53 8.42s-6.72 10.16-7.27 10.51c-0.4 0.25 3.09 3.94 10.44 11.07h-14.7-14.7v-15-15z"></path><path id="c" d="m46.99 0c0.5 0 15.01 28.79 15.01 29.89 0 0.18-30 0.12-30 0 0-1 14.5-29.89 14.99-29.89z"></path><path id="b" d="m0 0h14.7c4.85 0 11.2 2.29 11.53 8.42s-6.72 10.16-7.27 10.51c-0.4 0.26 3.09 3.95 10.44 11.07h-14.7-14.7v-15-15z"></path><path id="i" d="m30 94.98c-7.29 0.03-11.24 0.03-11.87 0-10.19-0.29-18.13-7.59-18.13-15.09 0-7.49 8.51-14.89 18.13-14.89h11.87v14.99 14.99z"></path></defs><use fill="#ffffff" xlink:href="#a"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#a"></use><use fill="#ffffff" xlink:href="#k"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#k"></use><use fill="#ffffff" xlink:href="#f"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#f"></use><use fill="#ffffff" xlink:href="#d"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#d"></use><use fill="#ffffff" xlink:href="#m"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#m"></use><use fill="#ffffff" xlink:href="#e"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#e"></use><use fill="#ffffff" xlink:href="#g"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#g"></use><use fill="#ffffff" xlink:href="#n"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#n"></use><use fill="#ffffff" xlink:href="#h"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#h"></use><use fill="#ffffff" xlink:href="#o"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#o"></use><use fill="#ffffff" xlink:href="#l"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#l"></use><use fill="#ffffff" xlink:href="#p"></use><use fill="#ffffff" xlink:href="#j"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#j"></use><use fill="#ffffff" xlink:href="#c"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#c"></use><use fill="#ffffff" xlink:href="#b"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#b"></use><use fill="#ffffff" xlink:href="#i"></use><use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#i"></use></svg>
            </div>
        </div>
        <div class="mm-navbar mm-navbar-size-1">
            <div class="mm-search">
                <input placeholder="Search" type="text" autocomplete="off">
            </div>
        </div>
    </div>
    <div class="mm-panels">
        <div class="mm-panel mm-opened mm-iconpanel-0" id="mm-2">
            <a href="#mm-2" class="mm-subblocker" tabindex="0"></a>
            <div class="mm-navbar" aria-hidden="true"><a class="mm-title" tabindex="-1">Menu</a></div>
            <ul class="mm-listview">
                <li><a href="" tabindex="0">Home</a></li>
                <li><a href="" tabindex="0">Park</a></li>
                <li><a href="" tabindex="0">Opening Hours &amp; Prices</a></li>
                <li><a href="" tabindex="0">Private Bookings/Parties</a></li>
                <li><a href="" tabindex="0">Online booking</a></li>
                <li><a href="" tabindex="0">Membership</a></li>
                <li><a href="" tabindex="0">Shop</a></li>
                <li><a href="" tabindex="0">Events</a></li>
                <li><a href="" tabindex="0">Contact</a></li>
            </ul>
            <div class="mm-noresultsmsg mm-hidden">No results found.</div>
        </div>
    </div>
          
    <div class="mm-navbars-bottom">
        <div class="mm-navbar mm-navbar-size-1 mm-navbar-content-3">
            <a href=""><i class="fa fa-envelope" aria-hidden="true"></i></a>
            <a href="https://www.instagram.com/rampworldcardiff"><i class="fab fa-instagram" aria-hidden="true"></i></a>
            <a href="http://www.facebook.com/RampWorldCardiff"><i class="fab fa-facebook" aria-hidden="true"></i></a>
        </div>
    </div>
    <button class="mm-tabend" tabindex="0" type="button" aria-hidden="true"></button>
</nav>
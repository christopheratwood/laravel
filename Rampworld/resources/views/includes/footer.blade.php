<footer>
	<div class="footer-left">
    {!! $logo !!}

		<!-- Footer Navigation start -->
		<p class="footer-links">
			<a href="/">Home</a>
			&bull;
			<a href="">News</a>
			&bull;
			<a href="">Opening</a>
			&bull;
			<a href="">Membership</a>
            &bull;
            <a href="">Online booking</a>
			&bull;
			<a href="">The Shop</a>
            &bull;
			<a href="">Events</a>
			&bull;
			<a href="">Contact Us</a>
		</p>
		<!-- Footer Navigation end -->

		<!-- Footer Website meta start -->
		<p class="footer-company-name">RampWorld Cardiff &copy; <?php echo date("Y"); ?></p>
		<p class="footer-company-name">Registed Charity Number 1152842</p>

        <a class="footer-company-name" href="/core">Login</a>
 

        <!-- Footer Website meta end -->
    </div>
    <!-- Footer addresses start -->
    <div class="footer-center">

        <div>
            <p class="fas fa-map-marker-alt"><a style="color: white !important;vertical-align: inherit;" href="//www.rampworldcardiff.co.uk/contact">RampWorld Cardiff,<br>Unit B,<br>Park Ty-Glas,<br>Llanishen,<br>Cardiff,<br>CF14 5DU</a></p>
        </div>
        <div>
            <p class="fas fa-envelope" ><a href="mailto:&#114;&#097;&#109;&#112;&#119;&#111;&#114;&#108;&#100;&#099;&#097;&#114;&#100;&#105;&#102;&#102;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;">&#114;&#097;&#109;&#112;&#119;&#111;&#114;&#108;&#100;&#099;&#097;&#114;&#100;&#105;&#102;&#102;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;</a></p>
        </div>

    </div>
    <!-- Footer addresses end -->
    
    <!-- Footer company info start -->
    <div class="footer-right">

        <p class="footer-company-about">

            <span>About RampWorld Cardiff</span>
            RampWorld Cardiff is run to provide an indoor recreational facility for modern wheeled sports in Cardiff, South Wales for young people with the object of improving their condition of life and in particular to provide facilities for BMX cycling, scooters, skateboarding and other such activities in a safe indoor environment.
        </p>
            
        <div class="footer-icons">
            <a href="https://www.facebook.com/RampWorldCardiff" class="fab fa-facebook-f" target="_blank" ></a>
            <a href="https://www.instagram.com/rampworldcardiff" class="fab fa-instagram" target="_blank" ></a>
        </div>        
    </div>
</footer>
@if(isset($content['title']) || isset($content['greeting']))
    <div class="jumbotron">
        @if(isset($content['title']))
            <h1>{{ $content['title'] }}</h1>
        @endif
        @if(isset($content['greeting']))
            <p>{{ $content['greeting'] }}</p>
        @endif
    </div>
@endif
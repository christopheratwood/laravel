<header>
	<div class="container-fluid clearfix">
		<ul id="navigationToggle" class="burger">
			<li></li>
			<li></li>
			<li></li>
		</ul>
		<div class="clearfix headerlogo">
			<a href="{{ url('/') }}">
        {!! $logo !!}
      </a>
		</div>
		<div class="clearfix nav-menu">
			<div class="header-navigation">
				<nav id="menu" class="navigation" role="navigation">
					<ul id="nav">
						<li class="menu-item"><a href="{{ route('homepage') }}">Home</a></li>
						<li class="menu-item"><a href="">Park</a></li>
						<li class="menu-item"><a href="">Opening Hours &amp; Prices</a></li>
						<li class="menu-item"><a href="">Private Bookings/Parties</a></li>
						<li class="menu-item"><a href="">Online booking</a></li>
						<li class="menu-item"><a href="">Membership</a></li>
						<li class="menu-item"><a href="">Shop</a></li>
						<li class="menu-item"><a href="{{ route('events') }}">Events</a></li>
						<li class="menu-item"><a href=">">Contact</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>
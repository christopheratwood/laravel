<meta charset="utf-8">
<title>Core - @yield('title')</title>

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="canonical" href="https://www.rampworldcardiff.co.uk/core/" />
<meta property="og:locale" content="en_GB" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ assets('css/rwcui.css') }}">
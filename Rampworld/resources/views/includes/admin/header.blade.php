<header>
	<div class="container-fluid clearfix">
		<ul id="navigationToggle" class="burger">
			<li></li>
			<li></li>
			<li></li>
		</ul>
		<div class="clearfix headerlogo">
			<a href="{{ url('/') }}">
        {!! $logo !!} Core
      </a>
		</div>
		{{ Auth::user()->privilege }}
		<div class="clearfix nav-menu">
			<div class="header-navigation">
				<nav id="menu" class="navigation" role="navigation">
					<ul id="nav">
						<li class="menu-item"><a href="{{ route('homepage') }}">Website</a></li>
						{{Auth::user()->privilege->name}} @if (Auth::user()->privilege->name === 1)
						<li class="menu-item"><a href="">Entry System</a></li>
						@endif
						<li class="menu-item"><a href="">Rota</a></li>
						<li class="menu-item"><a href="">Change details</a></li>
						<li class="menu-item"><a href="">Online booking</a></li>
						<li class="menu-item"><a href="">Membership</a></li>
						<li class="menu-item"><a href="">Shop</a></li>
						<li class="menu-item"><a href="">Events</a></li>
						<li class="menu-item"><a href=">">Contact</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>
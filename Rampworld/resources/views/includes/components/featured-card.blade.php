<div class="col-ms-12">
	<div class="card card-highlight">
		<div class="card-image "><img src="{{ asset($featured['image']) }}" class="img-responsive ">
			<h2 class="card-title">{{ $featured['title'] }}</h2>
		</div>
		<div class="card-content ">
			<p>{{ $featured['content'] }}</p>
		</div>
		@if(isset($featured['buttons']))
		<div class="card-action">
			@foreach($featured['buttons'] as $button)
				@if(isset($button['page_params']))
					<?php $url = route($button['page_name'], $button['page_params']);?>
				@else
					<?php $url = route($button['page_name']);?>
				@endif
				<a href="{{ $url }}" class="{{ $button['class'] }}">{{ $button['text'] }}</a>
			@endforeach
		</div>
		@endif
	</div>
</div>
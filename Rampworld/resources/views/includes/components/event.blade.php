<div class="col-md-3 col-sm-63 col-xs-6 col-ms-12">
  <div class="card ">
    <div class="card-content ">
      <p>{{$event->display_name}}</p>
    </div>
    <div class="card-action ">
      <table class="list list-with-icon">
        <tbody>
          <tr>
            <td><i class="far fa-calendar-alt fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
            <td><span>{{ \Carbon\Carbon::parse($event->date)->format('l \t\h\e jS \o\f F Y') }}</span></td>
          </tr>
          <tr>
            <td><i class="far fa-clock fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
            <td><span>{{ \Carbon\Carbon::parse($event->start_time)->format('g:i a') }} - {{ \Carbon\Carbon::parse($event->end_time)->format('g:i a') }}</span></td>
          </tr>
          <tr>
            <td><i class="fas fa-pound-sign fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
            <td><span>{{ '£'. $event->cost }}</span></td>
          </tr>
        </tbody>
      </table>
      <a class="btn btn-default btn-block" href="{{ route('viewEvent', ['event_name' => str_replace(' ', '-', $event->display_name)]) }}" style="margin: 0px">View event</a>
    </div>
  </div>
</div>

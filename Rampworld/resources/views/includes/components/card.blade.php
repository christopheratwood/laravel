<div class="@if(!isset($card['size'])) col-md-4 col-sm-6 col-xs-6 col-ms-12 @else {{$card['size']}} @endif">
	<div class="card ">
		@if( isset( $card['image'] ) )
			<div class="card-image ">
				<img class="img-responsive " src="{{ asset($card['image']) }}">
				@if(isset( $card['title'] ))
					<h2 class="card-title">{{ $card['title'] }}</h2>
				@endif
		</div>
		@endif

		<div class="card-content">
			@if( !isset( $card['image']) && isset( $card['title'] ) )
			<h3>{{ $card['title'] }}</h3>
			@endif 
			@if( isset( $card['content'] ) )
				<p>{{ $card['content'] }}</p>
			@endif
			
		</div>
		@if(isset($card['buttons']) || isset($card['list']))
			<div class="card-action ">
				@if(isset($card['before_list_text']))
					{!!$card['before_list_text']!!}
				@endif
				@if(isset($card['list']))
					<ul class="smart">
						@foreach($card['list'] as $item)
							<li>{{ $item }}</li>
						@endforeach
					</ul>
				@endif
				@if(isset($card['buttons']))
					@foreach($card['buttons'] as $button)
						<a class="{{ $button['class'] }}" href="@if( !isset($button['is_external'])){{ $button['page_name'] }}@else{{ $buttom['url'] }}@endif">{{ $button['text'] }}</a>
					@endforeach
				@endif
			</div>
		@endif
	</div>
</div>
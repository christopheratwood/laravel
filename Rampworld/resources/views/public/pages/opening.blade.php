@extends('layouts.default')
@section('content')
	@if(isset($featured))
		<div class="row">
			@include('includes.components.featured-card', $featured)
		</div>
	@endif
	@if(isset($cards))
		<div class="row">
			<h2>Featured pages</h2>
			@foreach($cards as $card)
				@include('includes.components.card', $card)
			@endforeach
		</div>
	@endif
	@if(count($events) > 0)
		<div class="row">
			<h2>Featured events</h2>
			@foreach($events as $event)
				@include('includes.components.event', $event)
			@endforeach
		</div>
	@endif
	@if(isset($information) > 0)
		<div class="row">
			<h2>Useful information</h2>
			@foreach($information as $card)
				@include('includes.components.card', $card)
			@endforeach
		</div>
	@endif
@endsection
@extends('layouts.default')
@section('content')
	@if(isset($featured))
		<div class="row">
			@include('includes.components.featured-card', $featured)
		</div>
	@endif
	@if(count($events) > 0)
		<div class="row">
			<h2>Upcoming events</h2>
			@foreach($events as $event)
				@include('includes.components.event', $event)
			@endforeach
		</div>
	@endif
@endsection
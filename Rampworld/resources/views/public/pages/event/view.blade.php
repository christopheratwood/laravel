@extends('layouts.default')
@section('content')
  <div class="row">
    <div class="col-md-4 col-xs-6 col-ms-12">
      <div class="event-image">
        <img style="width: 100%" src="{{ asset($event->image_portrait) }}">
      </div>
    </div>
    <div class="col-md-4 col-xs-6 col-ms-12">
      {{ $event->description }}
    </div>
    <div class="col-md-4 col-xs-6 col-ms-12">
      <section class="related-content">
        <div class="related-content-title">
          <p class="lead">Event information</p>
        </div>
        <div class="related-content-content">
          <div class="row">
            <div class="col-md-12">
              <table class="list list-with-icon">
                <tbody>
                    <tr>
                      <td><i class="far fa-calendar-alt fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
                      <td><span>{{ \Carbon\Carbon::parse($event->date)->format('l \t\h\e jS \o\f F Y') }}</span></td>
                    </tr>
                    <tr>
                      <td><i class="far fa-clock fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
                      <td><span>{{ \Carbon\Carbon::parse($event->start_time)->format('g:i a') }} - {{ \Carbon\Carbon::parse($event->end_time)->format('g:i a') }}</span></td>
                    </tr>
                    
                    <tr>
                      <td><i class="fas fa-ticket-alt fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
                      <td><span>
                        @if($event->details[0]['online_exclusive'])
                          Tickets for this event <strong>must</strong> purchased online.
                        @else
                          You can purchase tickets for this event online and instore
                        @endif
                      </span></td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-bicycle fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
                      <td><span>This event is only available for <?php echo $event->details[0]['discipline_exclusive'];?> riders.</span></td>
                    </tr>
                    @if($event->beginner)
                      <tr>
                        <td><i class="fas fa-ban fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
                        <td><span>This event is <strong>only</strong> for beginners.</span></td>
                      </tr>
                    @endif
                    <tr>
                      <td><i class="fas fa-pound-sign fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
                      <td><span>{{ '£'. $event->cost }}</span></td>
                    </tr>
                  </tbody>
                
              </table>
              @if($event->pass_exclusion)
                <div class="alert alert-danger"><p>If you have a <strong>week pass holder</strong>, you must purchase a ticket.  Special events are not covered within week passes.</p></div>
              @endif
              <h3>Book here</h3>
              <p>There are still tickets available  this event! You are able to use our online booking facilities to guarantee your place for this event.  There are a limited amount of tickets.</p>
              <a href="{{ route('online-booking-form', ['date' => $event->date]) }}" class="btn btn-lg btn-primary btn-block">Online booking</a>

            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
@endsection
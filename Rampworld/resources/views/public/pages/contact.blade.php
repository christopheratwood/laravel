@extends('layouts.default')
@section('content')
  <p class="lead">Need to get in contact with us? Use our simple form below and we will get back to within 48 hours</p>
  <div class="center">
    <contact-us></contact-us>
  </div> 

@endsection
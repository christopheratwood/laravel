@extends('layouts.default')
@section('content')
  <div class="c c-no-results">
    <div class="icon">
      <i class="far fa-frown fa-10x"></i>
      <h1>{{ $error['title'] }}</h1>
      <p>{{ $error['action'] }}<small>Error code: {{ $error['code'] }}</small></p>
    </div>
  </div>
@endsection
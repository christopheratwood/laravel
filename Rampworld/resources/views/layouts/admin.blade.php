<!DOCTYPE html>
<html {{ app()->getLocale() }}>
<head>
    @include('includes.admin.head')
</head>
<body>
    @include('includes.admin.header')
    
    <div class="container" id="app">
        @include('includes.title')
        @yield('content')
    </div>
    @include('includes.mobile-navigation')
    @include('includes.footer')
    <script src="{{ 'js/vue.js' }}"></script>
    <script src="{{ 'js/app.js' }}"></script>
</body>
</html>
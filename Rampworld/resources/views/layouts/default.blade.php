<!DOCTYPE html>
<html {{ app()->getLocale() }}>
<head>
    @include('includes.head')
</head>
<body>
    @include('includes.header')
    
    <div class="container" id="app">
        @include('includes.title')
        @yield('content')
    </div>
    @include('includes.mobile-navigation')
    @include('includes.footer')
    
    <script src="{{ asset('js/vue.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
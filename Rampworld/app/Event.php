<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    public function scopeIsFeaturedOnHomepage($query) {
        return $query->where('homepageFeatured', 1);
    }

    public function scopeIsFeaturedOnEventsHomepage($query) {
        return $query->where('eventsHomepageFeatured', 1);
    }

    public function scopeLatest($query) {
        return $query->orderBy('date', 'desc')->orderBy('start_time', 'asc');
    }

}

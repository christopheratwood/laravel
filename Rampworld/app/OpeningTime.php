<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Setting;

class OpeningTime extends Model
{
    //
    public function scopeToday($query) {
        return $query->where('day', date('N') );
    }
    protected $fillable = [
        'day', 'open_time', 'close_time',
    ];
}

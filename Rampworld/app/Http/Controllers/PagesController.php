<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\Pages;

class PagesController extends Controller
{
	public function index() {
	

		$data = array(
			'meta' => [
				'title' => 'Home'
			],
			'content' => [
				'title' => 'Welcome to RampWorld Cardiff',
				'greeting'  => 'Wales\' Largest Indoor Skatepark in the heart of Cardiff'
			],
			'featured' => array(
				'title' => 'Brand new! Online booking',
				'image' => 'images/featured/online-booking.jpg',
				'content' => 'RampWorld Cardiff now offers the ability to pre book your sessions at RampWorld Cardiff! The service is available to all RampWorld Cardiff members with a valid membership number.',
				'buttons' => [
					0 => [
						'class' => 'btn btn-primary btn-block',
						'page_name' => 'homepage',
						'text' => 'Book now!'
					]
				]
			),
			'cards' => array(
				0 => [
					'title' => 'RampWorld Membership',
					'image' => 'images/homepage/membership.jpg',
					'content' => 'RampWorld Cardiff Registration Form',
					'buttons' => [
						0 => [
							'class' => 'btn btn-default',
							'page_name' => 'homepage',
							'text' => 'Learn More'
						],
						1 => [
							'class' => 'btn btn-primary',
							'page_name' => 'homepage',
							'text' => 'Register here'
						]
					]
				],
				1 => [
					'title' => 'Opening Times and Prices',
					'image' => 'images/homepage/opening.jpg',
					'content' => 'Please visit for a full breakdown of opening times and prices',
					'buttons' => [
						0 => [
							'class' => 'btn btn-default',
							'page_name' => 'homepage',
							'text' => 'View opening hours and pricing'
						]
					]
				],
				2 => [
					'title' => 'The Skatepark',
					'image' => 'images/homepage/park.jpg',
					'content' => 'Fancy a look at the skatepark? We have pictures of every section within the skatepark!',
					'buttons' => [
						0 => [
							'class' => 'btn btn-default',
							'page_name' => 'homepage',
							'text' => 'View opening hours and pricing'
						]
					]
				], 
			),
			'events' => Event::isFeaturedOnHomepage()->latest()->take(4)->get(),
			'information' => [
				0 => [
					'size' => 'col-sm-6 col-xs-12 col-ms-12',
					'title' => 'RampWorld Cardiff Rules',
					'before_list_text' => '<p>Below are the main rules that everyone must adhere too.</p>',
					'list' => [
						'All scooter riders and everyone under the age of 16 must wear a helmet at RampWorld Cardiff. No Exceptions.',
						'All Scooters and Bikes must have Bar end plugs fitted to their handlebars, we sell them in the park for £4 a pair.',
						'We do not rent helmets, however, helmets start from £20.',
						'No fold-up scooters, no three-wheeled scooters.',
						'RampWorld Cardiff has a new membership registration system. It\'s a free service. All customers will need to register as members and will be allocated a unique membership number. Please provide this number at reception fast entry.'
					]
				],
				1 => [
					'size' => 'col-sm-6 col-xs-12 col-ms-12',
					'title' => 'Information',
					'before_list_text' => '<p>RampWorldCardiff is a nonprofit making charity set up to provide a progressive indoor Extreme sports training facility for South Wales. There are shops, a fun swimming pool, a trampoline park and a soft play facility all within five minutes walk of RampWorld.</p><p>There is no need to book in advance, but all participants must register</p>',
					'list' => [
					]
				],
			]
			
		);

		return view('public.pages.home')->with($data);
	}

	public function contact() {
		$data  = array(
			'meta' => [
				'title' => 'Contact us'
			], 
			'content' => [
				'title' => 'How to contact us'
			]
		);

		return view('public.pages.contact')->with($data);
	}
   
}

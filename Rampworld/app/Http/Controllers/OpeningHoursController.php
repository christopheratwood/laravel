<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OpeningTime;
use App\Setting;

class OpeningHoursController extends Controller
{
  public function index() {
    $featured = [];
		var_dump(Setting::isEqualTo('week_commencing_sunday', "1"));
		
    //$openingTimes = OpeningTime::all()->order('day';

    $opening = stdClass();
    
    foreach($openingTimes as $time)

    $data = array(
			'meta' => [
				'title' => 'Opening hours'
			],
			'content' => [
				'title' => 'Opening hours'
			],
			'weekCommencingSunday' => Setting::isEqualTo('week_commencing_sunday', "1"),
			'today' => OpeningTime::today(),
			'times' => OpeningTime::all()->order('day', 'desc')->order('open_time', 'desc')->order('close_time', 'desc')
		
		);

		return view('public.pages.opening')->with($data);
	} 

	public function view($event_name) {
		$data = array(
			'meta' => [
				'title' => str_replace('-', ' ', $event_name)
			],
			'content' => [
				'title' => str_replace('-', ' ', $event_name)
			],
			'event' => Event::where('display_name', str_replace('-', ' ', $event_name))->first(),
		
		);
		if( empty($data['event'])) {
			$page = array(
				'meta' => [
					'title' => 'This event could not be found'
				], 

				'error' => [
					'code' => 'EV01',
					'title' => 'The event you have attempted view has either been remove or does not exists.',
					'action' => 'Please use our events navigation to find the event you were looking for.'
				]
				);
			return view('public.error.404', $page);
		}
		return view('public.pages.event.view')->with($data);
	}
}

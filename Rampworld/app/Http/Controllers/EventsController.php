<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

class EventsController extends Controller
{
    public function index() {
		$featured = [];
		$event_featured = Event::isFeaturedOnEventsHomepage()->first();

		if($event_featured) {
			$featured = [
				'title' => $event_featured->display_name,
				'image' => $event_featured->image_landscape,
				'content' => $event_featured->teaser,
				'buttons' => [
					0 => [
						'class' => 'btn btn-primary btn-block',
						'text'	=> 'View event',
						'page_name' => 'viewEvent',
						'page_params' => ['event_name' => str_replace(' ', '-', $event_featured->display_name)]
					]
				]
			];
		}

		$data = array(
			'meta' => [
				'title' => 'Events'
			],
			'content' => [
				'title' => 'Events'
			],
			'featured' => $featured,
			'events' => Event::latest()->get(),
		
		);

		return view('public.pages.event.homepage')->with($data);
	} 

	public function view($event_name) {
		$data = array(
			'meta' => [
				'title' => str_replace('-', ' ', $event_name)
			],
			'content' => [
				'title' => str_replace('-', ' ', $event_name)
			],
			'event' => Event::where('display_name', str_replace('-', ' ', $event_name))->first(),
		
		);
		if( empty($data['event'])) {
			$page = array(
				'meta' => [
					'title' => 'This event could not be found'
				], 

				'error' => [
					'code' => 'EV01',
					'title' => 'The event you have attempted view has either been remove or does not exists.',
					'action' => 'Please use our events navigation to find the event you were looking for.'
				]
				);
			return view('public.error.404', $page);
		}
		return view('public.pages.event.view')->with($data);
	}
}

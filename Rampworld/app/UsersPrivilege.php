<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersPrivilege extends Model
{
    //
    protected $fillable = [
        'name', 'dashboard', 'dashboard_advance', 'change_membership_details', 'entry_system', 'advance_entry', 'create_promo_email', 'booking_view','booking_refund', 'booking_edit', 'booking_delete', 'member_edit', 'member_view', 'member_view_statistics', 'member_delete', 'entries_view', 'session_view', 'session_view_statistics', 'session_create', 'session_edit', 'session_delete', 'holiday_view', 'holiday_view_statistics', 'holiday_create', 'holiday_delete', 'holiday_edit', 'premises_add', 'premises_advance', 'settings', 'reports', 'advance_membership_view', 'advance_membership_view_statistics', 'advance_membership_edit', 'advance_membership_create', 'advance_membership_assign', 'advance_membership_delete', 'terms_view', 'terms_edit', 'terms_edit', 'event_view', 'event_vieww', 'event_view_statistics', 'event_delete', 'event_create', 'event_edit', 'rota_view', 'rota_view_seen', 'rota_create', 'rota_edit', 'privilege_cretae', 'privilege_assign', 'privilege_delete', 'privilege_view', 'privilege_view_statistics'
    ];
    public function users() {
        return $this->hasMany('App\User');
    }
}

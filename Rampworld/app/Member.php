<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
	//

	public function consent() {
		return $this->hasOne('App\MemberConsent');
  }
  public function user() {
		return $this->hasOne('App\User');
	}
}

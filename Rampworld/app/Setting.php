<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillabled = ['name', 'value'];

    public function scopeIsEqualTo($query, $settingName, $value) {
        $result = $query->where('name', $settingName)
                     ->where('value', $value)->first();
        return (empty($result)) ? false: true;
    }
}

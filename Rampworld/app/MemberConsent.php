<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberConsent extends Model
{
    //
    public function member() {
		return $this->belongsTo('App\Member');
	}
}

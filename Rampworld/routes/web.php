<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('homepage');

Auth::routes();

Route::get('/core/dashboard', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/users', function() {
    $users = App\User::all();
    if(count($users) > 0) {
    foreach($users as $user) {
        echo $user->member->consent;
    }}
});

Route::get('/events', 'EventsController@index')->name('events');
Route::get('/events/{event_name}', 'EventsController@view')->name('viewEvent');

Route::get('/online-booking/', 'BookingController@view')->name('online-booking');
Route::get('/online-booking/book', 'BookingController@book')->name('online-booking-form');
Route::get('/online-booking/book/{date}', 'BookingController@book')->name('online-booking-form');

Route::get('/opening-hours', 'OpeningHoursController@index')->name('Opening-hours');

Route::get('/contact', 'PagesController@contact')->name('Contact');
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Privilege extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_privilages', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('dashboard');
            $table->boolean('advance_dashboard');
            $table->boolean('change_membership_details');
            $table->boolean('entry_system');
            $table->boolean('advance_entry');
            $table->boolean('view_form');
            $table->boolean('delete_action');
            $table->boolean('modify_action');
            $table->boolean('resend_email');
            $table->boolean('create_promo_email');
            $table->timestamps();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_privilages');
    }
}

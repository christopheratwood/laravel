<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('forename', 100);
            $table->string('surname', 100);
            $table->char('gender',1);
            $table->date('dob');
            $table->string('email');
            $table->string('number', 16);
            $table->string('address_line_one', 50);
            $table->string('address_line_two', 50)->nullable();
            $table->string('address_city', 50)->nullable();
            $table->string('address_county', 50)->nullable();
            $table->string('address_postcode', 10);
            $table->string('discipline', 50);
            $table->tinyInteger('extertise');
            $table->text('medical_notes')->nullable();
            $table->string('verified', 50)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}

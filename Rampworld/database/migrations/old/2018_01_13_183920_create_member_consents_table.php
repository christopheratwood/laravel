<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberConsentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_consents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id');
            $table->string('name', 100);
            $table->string('number', 16);
            $table->string('address_line_one', 50);
            $table->string('address_postcode', 50);
            $table->date('dob');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_consents');
    }
}
